package com.example.buddyschooltestapp.activity;

/**
 * Structure interface for {@link ActivityMain}
 */
public interface IActivityBase
{
    /**
     * @return R.layout reference for layout which should be used as activity layout
     */
    int getLayoutId();

    /**
     * Place to setup all components
     */
    void setupComponents();

    /**
     * Place to setup logic
     */
    void setupLogic();
}
