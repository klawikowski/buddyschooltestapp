package com.example.buddyschooltestapp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Base Activity for all other activities
 * Contains all shared and reusable logic for activity
 */
public abstract class ActivityBase extends AppCompatActivity implements IActivityBase
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        setupComponents();
        setupLogic();
    }
}
