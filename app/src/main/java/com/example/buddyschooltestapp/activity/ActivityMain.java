package com.example.buddyschooltestapp.activity;

import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;

import com.example.buddyschooltestapp.R;

/**
 * Main Activity Class
 */
public class ActivityMain extends ActivityBase
{
    private Toolbar mToolbar;

    @Override
    public int getLayoutId()
    {
        return R.layout.activity_main;
    }

    @Override
    public void setupComponents()
    {
        mToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
    }

    @Override
    public void setupLogic()
    {
        setSupportActionBar(mToolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        //search logic is located in @link FragmentMainList
        return true;
    }
}
