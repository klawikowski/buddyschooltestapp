package com.example.buddyschooltestapp;

import android.app.Application;
import android.content.Context;

import com.example.buddyschooltestapp.data.DataHandler;
import com.example.buddyschooltestapp.data.IDataHandler;
import com.example.buddyschooltestapp.data.db.ManagerDB;
import com.example.buddyschooltestapp.data.ws.ManagerWS;
import com.squareup.leakcanary.LeakCanary;

/**
 * Main Application class
 */
public class ApplicationBuddy extends Application
{
    /**
     * Returns {@link IDataHandler} instance
     *
     * @param pContext
     * @return data handler instance
     */
    public static IDataHandler getDataHandler(Context pContext)
    {
        return DataHandler.getInstance(pContext, ManagerDB.getInstance(pContext), ManagerWS.getInstance(pContext));
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        LeakCanary.install(this);
    }

}
