package com.example.buddyschooltestapp.fragment;

import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.buddyschooltestapp.ApplicationBuddy;
import com.example.buddyschooltestapp.R;
import com.example.buddyschooltestapp.activity.ActivityMain;
import com.example.buddyschooltestapp.adapter.AdapterMainList;
import com.example.buddyschooltestapp.adapter.IAdapterClickListener;
import com.example.buddyschooltestapp.adapter.IDataProvider;
import com.example.buddyschooltestapp.data.IDataHandler;
import com.example.buddyschooltestapp.data.db.ManagerDB;
import com.example.buddyschooltestapp.data.model.Buddy;
import com.example.buddyschooltestapp.data.model.BuddyHelper;
import com.example.buddyschooltestapp.receivers.ConnectionStatus;
import com.example.buddyschooltestapp.utils.HelperConnection;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment presenting list of recipes in {@link ActivityMain}
 */
public class FragmentMainList extends FragmentBase implements IDataProvider<List<Buddy>>, IAdapterClickListener<Buddy>
{
    private LinearLayout mNoConnectionBar;
    private RecyclerView mRecyclerView;
    private AdapterMainList mAdapterMainList;
    private View mListMessageView;
    private View mProgressView;
    private String mQuery = "";

    private List<Buddy> mData = new ArrayList<>();

    @Override
    public int getLayoutId()
    {
        return R.layout.fragment_main_list;
    }

    @Override
    public void setupComponents(View pFragmentView)
    {
        mNoConnectionBar = (LinearLayout) pFragmentView.findViewById(R.id.fragment_main_list_wrapper_no_connection);

        mListMessageView = pFragmentView.findViewById(R.id.fragment_main_list_message_view);
        mProgressView = pFragmentView.findViewById(R.id.fragment_main_list_progress_view);


        mRecyclerView = (RecyclerView) pFragmentView.findViewById(R.id.fragment_main_list_recycler);
        mAdapterMainList = new AdapterMainList(getContext(), this, this, mListMessageView);
    }

    @Override
    public void setupLogic()
    {
        setupNoConnectionBarVisibility();
        setupLoadingView(false);
        setupRecyclerView();


        ManagerDB.getInstance(getContext()).getAllBuddies();
    }

    /**
     * Full setup of recycler view
     */
    private void setupRecyclerView()
    {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapterMainList);
    }


    /**
     * Shows or hides no connection status bar depends on connection availability
     */
    private void setupNoConnectionBarVisibility()
    {
        mNoConnectionBar.setVisibility(HelperConnection.isOnline(getContext()) ? View.GONE : View.VISIBLE);
    }

    /**
     * Shows or hides loading view displayed when webservice request is launched
     * Its connected with empty view and recycler view - we dont want to display them when loading data
     */
    private void setupLoadingView(boolean pIsVisible)
    {
        mProgressView.setVisibility(pIsVisible ? View.VISIBLE : View.GONE);
        mListMessageView.setVisibility(pIsVisible ? View.GONE : View.VISIBLE);
        mRecyclerView.setVisibility(pIsVisible ? View.GONE : View.VISIBLE);
    }

    @Subscribe
    /**
     * Method callback from Otto Bus for Connection Status
     * Callback : {@link com.example.buddyschooltestapp.receivers.ConnectionChangeReceiver}
     */
    public void connectionChanged(ConnectionStatus pConnectionStatus)
    {
        setupNoConnectionBarVisibility();
    }

    @Override
    public List<Buddy> getAdapterData()
    {
        return mData;
    }

    @Override
    public void onAdapterItemClick(int pPosition, Buddy pObject, View pView)
    {
        //yeah it might be prewarmed and prelaunched - but do we really need it?
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(getActivity(), Uri.parse(BuddyHelper.getBuddyProfileDetailsUrl(pObject)));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        //SearchView logic
        MenuItem searchItem = menu.findItem(R.id.menu_main_search);
        final SearchView searchView = new SearchView(((ActivityMain) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(searchItem, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(searchItem, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String query)
            {
                mQuery = query;
                hideKeyboard(searchView);
                setupLoadingView(true);
                getDataForQuery(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText)
            {
                mQuery = newText;
                //canceling the search ?
                if(newText.length() == 0)
                {
                    //clear data
                    mData.clear();
                    mAdapterMainList.notifyDataSetChanged();
                    setupLoadingView(false);
                    determineSearchResultsVsEmptyQuery();
                }
                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Requests DataHandler for data matching given query
     *
     * @param pQuery
     */
    private void getDataForQuery(String pQuery)
    {
        ApplicationBuddy.getDataHandler(getContext()).getBuddies(pQuery, new IDataHandler.IDataHandlerReturn<List<Buddy>>()
        {
            @Override
            public void onSuccess(List<Buddy> pReturn)
            {
                //ok we have data - lets clear previous collection, fill it with data and refresh the view
                mData.clear();
                //fill with new results
                mData.addAll(pReturn);
                mAdapterMainList.notifyDataSetChanged();
                setupLoadingView(false);
                //now we must determine one thing - we might have collection.size == 0, but query.size != 0
                //this means we are searching for something, but nobody wants to learn us ;)
                //lets make a simple trick, and change text string in view, so we dont have to make 1928301 views
                determineSearchResultsVsEmptyQuery();
            }
        });
    }

    /**
     * Determines state of search
     * Collection size must be 0 - otherwise we have results on list
     * If query is empty - it means we are not searching for anything
     * If query is not empty  - it means we dont have tutors
     */
    private void determineSearchResultsVsEmptyQuery()
    {
        if(mQuery.length() == 0)
        {
            ((TextView) mListMessageView.findViewById(R.id.fragment_main_list_message_view_text)).setText(R.string.SEARCH_FOR_BUDDIES);
        }
        else if(mQuery.length() != 0 && mData.size() == 0)
        {
            ((TextView) mListMessageView.findViewById(R.id.fragment_main_list_message_view_text)).setText(R.string.NO_DATA_FOUND);
        }
    }
}
