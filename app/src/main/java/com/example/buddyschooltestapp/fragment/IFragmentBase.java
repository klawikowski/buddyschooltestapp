package com.example.buddyschooltestapp.fragment;

import android.view.View;

/**
 * Structure interface for {@link FragmentBase}
 */
public interface IFragmentBase
{
    /**
     * Method should return R.layout reference to layout which should be used as fragment view
     *
     * @return R.layout reference to layout
     */
    int getLayoutId();

    /**
     * Method used to set or initialize all fragment view components
     *
     * @param pFragmentView
     */
    void setupComponents(View pFragmentView);


    /**
     * Methos used to setup fragment logic
     */
    void setupLogic();
}
