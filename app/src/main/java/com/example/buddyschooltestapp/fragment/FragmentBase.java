package com.example.buddyschooltestapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.example.buddyschooltestapp.utils.HelperBus;

/**
 * Base class for all fragments
 */
public abstract class FragmentBase extends Fragment implements IFragmentBase
{
    protected View mFragmentView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        mFragmentView = inflater.inflate(getLayoutId(), container, true);
        return mFragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        //needed to setup menu from fragment
        setHasOptionsMenu(true);
        setupComponents(mFragmentView);
        setupLogic();
    }

    @Override
    public void onStart()
    {
        super.onStart();
        HelperBus.getInstance().register(this);
    }


    @Override
    public void onStop()
    {
        super.onStop();
        HelperBus.getInstance().unregister(this);
    }

    /**
     * Hides software keyboard
     *
     * @param pView Focusable view which triggered keyboard display
     */
    protected void hideKeyboard(View pView)
    {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(pView.getWindowToken(), 0);
    }

}
