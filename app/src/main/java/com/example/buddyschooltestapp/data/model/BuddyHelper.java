package com.example.buddyschooltestapp.data.model;

import com.example.buddyschooltestapp.data.ws.ConfigEndpoint;

/**
 * Buddy {@link Buddy} pojo helper
 */
public class BuddyHelper
{
    /**
     * @param pBuddy
     * @return Valid buddy photo url
     */
    public static String getBuddyPhotoUrl(Buddy pBuddy)
    {
        return ConfigEndpoint.BUDDY_SCHOOL_PATH + pBuddy.getPhotoUrl();
    }

    /**
     * @param pBuddy
     * @return Valid buddy profile url
     */
    public static String getBuddyProfileDetailsUrl(Buddy pBuddy)
    {
        return ConfigEndpoint.BUDDY_SCHOOL_PATH + pBuddy.getProfileUrl();
    }

}
