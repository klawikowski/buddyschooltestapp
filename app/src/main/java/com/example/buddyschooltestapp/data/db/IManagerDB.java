package com.example.buddyschooltestapp.data.db;

import com.example.buddyschooltestapp.data.model.Buddy;

import java.util.List;

/**
 * Adapter interface for all possible database implementations
 */
public interface IManagerDB
{
    /**
     * Saves given buddies to database
     *
     * @param pBuddyList
     */
    void saveBuddies(List<Buddy> pBuddyList);

    /**
     * @param pQuery Collection of buddies matching to given query
     */
    List<Buddy> getBuddies(String pQuery);

    /**
     * Returns all buddies saved in database
     */
    List<Buddy> getAllBuddies();


    /**
     * Clears whole database
     */
    void clearDataBase();
}
