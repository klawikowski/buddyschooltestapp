package com.example.buddyschooltestapp.data;

import android.content.Context;

import com.example.buddyschooltestapp.data.db.IManagerDB;
import com.example.buddyschooltestapp.data.model.Buddy;
import com.example.buddyschooltestapp.data.ws.IManagerWS;
import com.example.buddyschooltestapp.utils.HelperConnection;

import java.util.List;

/**
 * Manager Data
 * Handles all in / out requests
 * Automatic data save, automatic ws requests and EZ generic callbacks
 * Take a look at dependency injection techniques
 */
public class DataHandler implements IDataHandler
{
    private static DataHandler sInstance;

    private IManagerWS mManagerWS;
    private IManagerDB mManagerDB;
    private Context mContext;

    /**
     * Private constructor
     *
     * @param pContext   Context
     * @param pManagerDB ManagerDB dependency
     * @param pManagerWS ManagerWS dependency
     */
    private DataHandler(Context pContext, IManagerDB pManagerDB, IManagerWS pManagerWS)
    {
        mManagerDB = pManagerDB;
        mManagerWS = pManagerWS;
        mContext = pContext;
    }

    /**
     * Singleton instance for DataHandler
     *
     * @param pManagerDB ManagerDB dependency
     * @param pManagerWS ManagerWS dependency
     * @return instance of DataHandler
     */
    public static DataHandler getInstance(Context pContext, IManagerDB pManagerDB, IManagerWS pManagerWS)
    {
        if(sInstance == null)
        {
            synchronized(DataHandler.class)
            {
                if(sInstance == null)
                {
                    sInstance = new DataHandler(pContext, pManagerDB, pManagerWS);
                }
            }
        }
        return sInstance;
    }

    @Override
    public IManagerWS getManagerWS()
    {
        return mManagerWS;
    }

    @Override
    public IManagerDB getManagerDB()
    {
        return mManagerDB;
    }

    @Override
    public void getBuddies(final String pQuery, final IDataHandlerReturn<List<Buddy>> pHandlerReturn)
    {
        //do we have internet ?
        if(HelperConnection.isOnline(mContext))
        {
            getManagerWS().searchBuddy(pQuery, new IManagerWS.IManagerWSResponse<List<Buddy>>()
            {
                @Override
                public void onSuccess(List<Buddy> pReturn)
                {
                    //ok we have response - lets save it to DB
                    getManagerDB().saveBuddies(pReturn);
                    //everything saved - lets return data
                    pHandlerReturn.onSuccess(pReturn);
                }

                @Override
                public void onError()
                {
                    //error - parsing error or sth like that - lets return the cache
                    pHandlerReturn.onSuccess(getManagerDB().getBuddies(pQuery));
                }
            });
        }
        // we dont ;( - return cache
        else
        {
            pHandlerReturn.onSuccess(getManagerDB().getBuddies(pQuery));
        }
    }
}
