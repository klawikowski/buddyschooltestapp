package com.example.buddyschooltestapp.data.ws;

import com.example.buddyschooltestapp.data.model.Buddy;

import java.util.List;

/**
 * Adapter interface for all possible webservice implementations
 */
public interface IManagerWS
{
    void searchBuddy(String pQuery, IManagerWSResponse<List<Buddy>> pCallback);


    /**
     * Callback interface
     *
     * @param <T> Type of return
     */
    interface IManagerWSResponse<T>
    {
        void onSuccess(T pReturn);

        void onError();
    }
}
