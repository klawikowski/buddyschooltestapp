package com.example.buddyschooltestapp.data.ws.retrofit;

import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;

import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;

/**
 * Http Client wrapper
 * Ready for any possible modifications
 * Custom headers / url polling etc etc
 */
public class RetrofitClient extends OkClient
{
    public RetrofitClient(OkHttpClient pOkHttpClient)
    {
        super(pOkHttpClient);
    }

    @Override
    public Response execute(Request request) throws IOException
    {
        return super.execute(request);
    }
}
