package com.example.buddyschooltestapp.data;

import com.example.buddyschooltestapp.data.db.IManagerDB;
import com.example.buddyschooltestapp.data.model.Buddy;
import com.example.buddyschooltestapp.data.ws.IManagerWS;

import java.util.List;

/**
 * Adapter interface for all data handler implementations
 */
public interface IDataHandler
{
    /**
     * @return IManagerWS instance
     */
    IManagerWS getManagerWS();

    /**
     * @return IManagerDB Instance
     */
    IManagerDB getManagerDB();


    void getBuddies(String pQuery, IDataHandlerReturn<List<Buddy>> pReturn);

    /**
     * Callback interface
     *
     * @param <T> Type of return
     */
    interface IDataHandlerReturn<T>
    {
        void onSuccess(T pReturn);
    }
}
