package com.example.buddyschooltestapp.data.ws;

import android.content.Context;

import com.example.buddyschooltestapp.BuildConfig;
import com.example.buddyschooltestapp.data.model.Buddy;
import com.example.buddyschooltestapp.data.ws.retrofit.IRetrofitAdapter;
import com.example.buddyschooltestapp.data.ws.retrofit.RetrofitClient;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.realm.RealmObject;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Manager WS
 * Responsible for all WS calls, parsing and passing data through
 */
public class ManagerWS implements IManagerWS
{
    private static ManagerWS sInstance;
    private Context mContext;

    //retrofit adapter
    private IRetrofitAdapter mApiEndpoint;

    //http client
    private RetrofitClient mClient;

    /**
     * Private constructor
     *
     * @param pContext Context
     */
    private ManagerWS(Context pContext)
    {
        mContext = pContext;

        //client init
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

        //wrapping client into our dedicated client
        mClient = new RetrofitClient(okHttpClient);

        // GSON can parse the data.
        // Note there is a bug in GSON 2.5 that can cause it to StackOverflow when working with RealmObjects.
        // To work around this, use the ExclusionStrategy below or downgrade to 1.7.1
        // See more here: https://code.google.com/p/google-gson/issues/detail?id=440
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy()
                {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f)
                    {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz)
                    {
                        return false;
                    }
                })
                .create();

        //creating adapter
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ConfigEndpoint.BUDDY_SCHOOL_ENDPOINT_PATH)
                .setClient(mClient)
                .setConverter(new GsonConverter(gson))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .build();

        //init
        mApiEndpoint = restAdapter.create(IRetrofitAdapter.class);
    }

    /**
     * Singleton instance
     *
     * @param pContext
     * @return instance of ManagerWS
     */
    public static ManagerWS getInstance(Context pContext)
    {
        if(sInstance == null)
        {
            synchronized(ManagerWS.class)
            {
                if(sInstance == null)
                {
                    sInstance = new ManagerWS(pContext);
                }
            }
        }
        return sInstance;
    }


    @Override
    public void searchBuddy(String pQuery, final IManagerWSResponse<List<Buddy>> pCallback)
    {
        mApiEndpoint.getBuddies(pQuery, new Callback<List<Buddy>>()
        {
            @Override
            public void success(List<Buddy> pBuddies, Response response)
            {
                pCallback.onSuccess(pBuddies);
            }

            @Override
            public void failure(RetrofitError error)
            {
                pCallback.onError();
            }
        });
    }
}
