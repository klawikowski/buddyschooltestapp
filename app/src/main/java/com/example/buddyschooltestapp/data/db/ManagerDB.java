package com.example.buddyschooltestapp.data.db;

import android.content.Context;

import com.example.buddyschooltestapp.data.model.Buddy;

import java.util.ArrayList;
import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Manager DataBase
 * Responsible for all model load / save
 */
public class ManagerDB implements IManagerDB
{
    private static ManagerDB sInstance;
    private Context mContext;

    /**
     * Private constructor
     *
     * @param pContext Context
     */
    private ManagerDB(Context pContext)
    {
        mContext = pContext;
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(pContext).build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    /**
     * Singleton instance
     *
     * @param pContext
     * @return instance of ManagerDB
     */
    public static ManagerDB getInstance(Context pContext)
    {
        if(sInstance == null)
        {
            synchronized(ManagerDB.class)
            {
                if(sInstance == null)
                {
                    sInstance = new ManagerDB(pContext);
                }
            }
        }
        return sInstance;
    }

    @Override
    public void saveBuddies(List<Buddy> pBuddyList)
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(pBuddyList);
        realm.commitTransaction();
    }

    @Override
    public List<Buddy> getBuddies(String pQuery)
    {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Buddy.class).contains("mTitle", pQuery, Case.INSENSITIVE).findAll();
    }

    @Override
    public List<Buddy> getAllBuddies()
    {
        Realm realm = Realm.getDefaultInstance();
        ArrayList<Buddy> result = new ArrayList<>();
        result.addAll(realm.copyFromRealm(realm.where(Buddy.class).findAll()));
        return result;
    }

    @Override
    public void clearDataBase()
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(Buddy.class);
        realm.commitTransaction();
    }
}
