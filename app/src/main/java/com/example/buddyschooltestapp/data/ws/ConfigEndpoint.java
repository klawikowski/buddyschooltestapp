package com.example.buddyschooltestapp.data.ws;

/**
 * Simple endpoint config
 */
public class ConfigEndpoint
{
    //used for all endpoint calls
    public static final String BUDDY_SCHOOL_ENDPOINT_PATH = "https://buddyschool.com/partial";
    //used for all image / profile url calls
    public static final String BUDDY_SCHOOL_PATH = "https://buddyschool.com/";
}
