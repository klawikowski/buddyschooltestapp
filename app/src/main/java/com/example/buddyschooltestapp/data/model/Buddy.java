package com.example.buddyschooltestapp.data.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Buddy POJO
 */
public class Buddy extends RealmObject
{
    @PrimaryKey
    @SerializedName("teacher_login")
    private String mLogin;
    @SerializedName("teacher_photo")
    private String mPhotoUrl;
    @SerializedName("profile_title")
    private String mTitle;
    @SerializedName("profile_price")
    private String mPrice;
    @SerializedName("profile_url")
    private String mProfileUrl;

    public Buddy(String pLogin, String pPhotoUrl, String pTitle, String pPrice, String pProfileUrl)
    {
        mLogin = pLogin;
        mPhotoUrl = pPhotoUrl;
        mTitle = pTitle;
        mPrice = pPrice;
        mProfileUrl = pProfileUrl;
    }

    /**
     * Constructor
     */
    public Buddy()
    {

    }

    public String getLogin()
    {
        return mLogin;
    }

    public String getPhotoUrl()
    {
        return mPhotoUrl;
    }

    public String getTitle()
    {
        return mTitle;
    }

    public String getPrice()
    {
        return mPrice;
    }

    public String getProfileUrl()
    {
        return mProfileUrl;
    }
}
