package com.example.buddyschooltestapp.data.ws.retrofit;


import com.example.buddyschooltestapp.data.model.Buddy;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Retrofit Endpoint Api
 */
public interface IRetrofitAdapter
{
    @GET("/get-search-results")
    void getBuddies(@Query("keyword") String pQuery, Callback<List<Buddy>> pCallback);
}
