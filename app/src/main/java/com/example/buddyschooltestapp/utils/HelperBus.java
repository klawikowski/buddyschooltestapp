package com.example.buddyschooltestapp.utils;

import com.squareup.otto.Bus;

/**
 * Helper class for Otto bus
 */
public class HelperBus
{
    private static final Bus BUS = new Bus();

    public static Bus getInstance()
    {
        return BUS;
    }
}
