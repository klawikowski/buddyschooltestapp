package com.example.buddyschooltestapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Helper class for connection status
 */
public class HelperConnection
{

    /**
     * Checks current connection status
     *
     * @param pContext Context
     * @return bool for connection status
     */
    public static boolean isOnline(Context pContext)
    {
        ConnectivityManager cm = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in air plan mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }
}
