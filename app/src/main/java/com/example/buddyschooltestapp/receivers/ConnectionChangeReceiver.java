package com.example.buddyschooltestapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.buddyschooltestapp.utils.HelperBus;
import com.example.buddyschooltestapp.utils.HelperConnection;

/**
 * Broadcast receiver for connection changes
 */
public class ConnectionChangeReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        // ok we have info that connection has been changed - lets broadcast it!
        // wrap conn info into class, and pass it through otto
        HelperBus.getInstance().post(new ConnectionStatus(HelperConnection.isOnline(context)));
    }
}
