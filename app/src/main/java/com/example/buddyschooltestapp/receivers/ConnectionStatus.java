package com.example.buddyschooltestapp.receivers;

/**
 * Class used to publish connection status through otto bus
 */
public class ConnectionStatus
{
    public boolean isConnected()
    {
        return mConnectionStatus;
    }

    private boolean mConnectionStatus;

    public ConnectionStatus(boolean pConnectionStatus)
    {
        mConnectionStatus = pConnectionStatus;
    }
}
