package com.example.buddyschooltestapp.adapter;

import android.view.View;

/**
 * Generic Adapter Click Listener
 */
public interface IAdapterClickListener<T>
{
    /**
     * Adapter Item Click Callback
     *
     * @param pPosition index of item clicked
     * @param pObject   object from which cell was created - pojo
     * @param pView     view of cell
     */
    void onAdapterItemClick(int pPosition, T pObject, View pView);
}
