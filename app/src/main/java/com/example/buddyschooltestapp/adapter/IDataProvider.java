package com.example.buddyschooltestapp.adapter;

/**
 * Generic data provider for all adapters
 */
public interface IDataProvider<T>
{
    /**
     * @return Data for adapter
     */
    T getAdapterData();
}
