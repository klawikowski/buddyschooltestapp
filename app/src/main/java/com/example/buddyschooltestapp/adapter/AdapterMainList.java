package com.example.buddyschooltestapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.buddyschooltestapp.R;
import com.example.buddyschooltestapp.adapter.viewholder.ViewHolderBuddy;
import com.example.buddyschooltestapp.data.model.Buddy;
import com.example.buddyschooltestapp.data.model.BuddyHelper;

import java.util.List;

/**
 * Adapter for {@link com.example.buddyschooltestapp.fragment.FragmentMainList} Recycler View
 * Display simple cell filled with {@link Buddy} model
 */
public class AdapterMainList extends RecyclerView.Adapter<ViewHolderBuddy>
{

    private Context mContext;
    private IDataProvider<List<Buddy>> mDataProvider;
    private IAdapterClickListener<Buddy> mAdapterClickListener;
    private View mEmptyView;

    public AdapterMainList(Context pContext, @NonNull IDataProvider<List<Buddy>> pDataProvider, @NonNull IAdapterClickListener<Buddy> pAdapterClickListener, @NonNull View pEmptyView)
    {
        mContext = pContext;
        mDataProvider = pDataProvider;
        mAdapterClickListener = pAdapterClickListener;
        mEmptyView = pEmptyView;
    }

    @Override
    public ViewHolderBuddy onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new ViewHolderBuddy(LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_buddy, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolderBuddy holder, final int position)
    {
        Buddy buddy = mDataProvider.getAdapterData().get(holder.getAdapterPosition());
        if(buddy != null)
        {
            holder.getTitle().setText(buddy.getTitle());
            holder.getLogin().setText(buddy.getLogin());
            holder.getPrice().setText(buddy.getPrice());

            Glide.with(mContext).load(BuddyHelper.getBuddyPhotoUrl(buddy)).into(holder.getPhoto());

            holder.getCellView().setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(mAdapterClickListener != null)
                    {
                        mAdapterClickListener.onAdapterItemClick(holder.getAdapterPosition(), mDataProvider.getAdapterData().get(holder.getAdapterPosition()), holder.getCellView());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount()
    {
        if(mDataProvider != null)
        {
            int count = mDataProvider.getAdapterData().size();
            mEmptyView.setVisibility(count == 0 ? View.VISIBLE : View.GONE);
            return mDataProvider.getAdapterData().size();
        }
        return 0;
    }
}
