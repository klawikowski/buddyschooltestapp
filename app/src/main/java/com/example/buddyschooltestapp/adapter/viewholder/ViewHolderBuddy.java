package com.example.buddyschooltestapp.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.buddyschooltestapp.R;

/**
 * ViewHolder for Buddy List Cell used in {@link com.example.buddyschooltestapp.adapter.AdapterMainList}
 */
public class ViewHolderBuddy extends RecyclerView.ViewHolder
{
    private TextView mLogin;
    private ImageView mPhoto;
    private TextView mTitle;
    private TextView mPrice;
    private View mCellView;

    public ViewHolderBuddy(View itemView)
    {
        super(itemView);
        mCellView = itemView;
        mLogin = (TextView) itemView.findViewById(R.id.cell_buddy_login);
        mTitle = (TextView) itemView.findViewById(R.id.cell_buddy_title);
        mPrice = (TextView) itemView.findViewById(R.id.cell_buddy_price);
        mPhoto = (ImageView) itemView.findViewById(R.id.cell_buddy_photo);

    }

    public TextView getLogin()
    {
        return mLogin;
    }

    public ImageView getPhoto()
    {
        return mPhoto;
    }

    public TextView getTitle()
    {
        return mTitle;
    }

    public TextView getPrice()
    {
        return mPrice;
    }

    /**
     * @return View of cell
     */
    public View getCellView()
    {
        return mCellView;
    }
}
