package com.example.buddyschooltestapp;

import android.test.ApplicationTestCase;

import com.example.buddyschooltestapp.data.db.ManagerDB;
import com.example.buddyschooltestapp.data.model.Buddy;
import com.example.buddyschooltestapp.data.ws.IManagerWS;
import com.example.buddyschooltestapp.data.ws.ManagerWS;

import java.util.ArrayList;
import java.util.List;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationBuddyTest extends ApplicationTestCase<ApplicationBuddy>
{
    public ApplicationBuddyTest()
    {
        super(ApplicationBuddy.class);
    }

    public ApplicationBuddyTest(Class<ApplicationBuddy> applicationClass)
    {
        super(applicationClass);
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        createApplication();
    }

    public void testApplicationBuddy() throws Exception
    {
        //checking clear data base
        ManagerDB.getInstance(getSystemContext()).clearDataBase();
        assertEquals(0, ManagerDB.getInstance(getSystemContext()).getAllBuddies().size());

        ArrayList<Buddy> testCollection = new ArrayList<>();
        //adding 3 records
        for(int i = 0; i < 3; i++)
        {
            Buddy testBuddy = new Buddy("" + i, "" + i, "" + i, "" + i, "" + i);
            testCollection.add(testBuddy);
        }
        ManagerDB.getInstance(getSystemContext()).saveBuddies(testCollection);

        //checking return of whole collection
        assertEquals(3, ManagerDB.getInstance(getSystemContext()).getAllBuddies().size());

        //checking finding detailed buddy
        assertEquals(1, ManagerDB.getInstance(getSystemContext()).getBuddies("1").size());


        //checking ws parsing data for test given in application documentation
        ManagerWS.getInstance(getSystemContext()).searchBuddy("English Language-Business, Grammar, Test Prep, Conversation, Slang, All Levels; Teacher Training for student teachers of English", new IManagerWS.IManagerWSResponse<List<Buddy>>()
        {
            @Override
            public void onSuccess(List<Buddy> pReturn)
            {
                assertEquals(1, pReturn.size());
            }

            @Override
            public void onError()
            {

            }
        });
    }
}